# - *- coding: utf- 8 - *-

def return_articles():

    articles_sport = [
        ['http://www.sueddeutsche.de/sport/fussball-em-england-vergeigts-in-letzter-minute-nur-gegen-russland-1.3029430',
         '48.137348','11.635810'],
        ['http://www.nw.de/sport/dsc_arminia_bielefeld/20821614_Rehms-Wechsel-zur-Arminia-konkretisiert-sich.html',
         '48.067800','11.613990']
    ]


    articles_politics = [
        ['http://www.merkur.de/lokales/muenchen-lk-nord/aschheim-feldkirchen/aschheim-befuerwortet-schlachthofplaene-6475592.html',
         '48.067800','11.613990'],
        ['http://www.abendzeitung-muenchen.de/inhalt.mazedonische-farbbeutel-revolution-farbe-farbe-farbe-bunter-protest-gegen-korruption.0598af60-4b0b-4cf9-94fa-748e1d9d0efd.html',
         '48.129910','11.530057']
    ]


    articles_enter = [
        ['http://twoinarow.com/2016/06/viehhof-kino',
         '48.137650','11.540885'],
        ['https://isarbierundsonne.com/2016/05/30/schoenstes-muenchen-kw-21-all-i-need/',
         '48.137650','11.540885']
    ]

    return articles_sport, articles_politics, articles_enter

def return_func2(articles_sport, articles_politics, articles_enter):

    articles_sport2 = [{'type': 'article',
                        'id': 'abc', 'title': 'England pennt in letzter Minute - nur 1:1 gegen Russland',
                        'message_text': 'http://www.sueddeutsche.de/sport/fussball-em-england-vergeigts-in-letzter-minute-nur-gegen-russland-1.3029430',
                        'url': 'http://www.sueddeutsche.de/sport/fussball-em-england-vergeigts-in-letzter-minute-nur-gegen-russland-1.3029430',
                        'hide_url': False,
                        'description': 'Sport'
                        },
                       {'type': 'article',
                        'id': 'abc2', 'title': 'Rehms wechsel zur Arminia konkretisiert sich',
                        'message_text': 'http://www.nw.de/sport/dsc_arminia_bielefeld/20821614_Rehms-Wechsel-zur-Arminia-konkretisiert-sich.html',
                        'url': 'http://www.nw.de/sport/dsc_arminia_bielefeld/20821614_Rehms-Wechsel-zur-Arminia-konkretisiert-sich.html',
                        'hide_url': False,
                        'description': 'Sport'
                        }
                       ]
    articles_pol2 = [{'type': 'article',
                        'id': 'abc', 'title': 'SPD befürwortet Schlachthofpläne'.encode('utf-8'),
                        'message_text': 'http://www.merkur.de/lokales/muenchen-lk-nord/aschheim-feldkirchen/aschheim-befuerwortet-schlachthofplaene-6475592.html',
                        'url': 'http://www.merkur.de/lokales/muenchen-lk-nord/aschheim-feldkirchen/aschheim-befuerwortet-schlachthofplaene-6475592.html',
                        'hide_url': False,
                        'description': 'Politics'
                        },
                       {'type': 'article',
                        'id': 'abc2', 'title': '"Farbe! Farbe! Farbe!": Bunter Protest gegen Korruption',
                        'message_text': 'http://www.abendzeitung-muenchen.de/inhalt.mazedonische-farbbeutel-revolution-farbe-farbe-farbe-bunter-protest-gegen-korruption.0598af60-4b0b-4cf9-94fa-748e1d9d0efd.html',
                        'url': 'http://www.abendzeitung-muenchen.de/inhalt.mazedonische-farbbeutel-revolution-farbe-farbe-farbe-bunter-protest-gegen-korruption.0598af60-4b0b-4cf9-94fa-748e1d9d0efd.html',
                        'hide_url': False,
                        'description': 'Politics'
                        }
                       ]

    articles_enter2 = [{'type': 'article',
                        'id': 'abc', 'title': 'Kino- und Kulturfestival auf dem Viehhof',
                        'message_text': 'http://twoinarow.com/2016/06/viehhof-kino',
                        'url': 'http://twoinarow.com/2016/06/viehhof-kino',
                        'hide_url': False,
                        'description': 'Entertainment'
                        },
                       {'type': 'article',
                        'id': 'abc2', 'title': 'SCHÖNSTES MÜNCHEN // KW 21 // ALL I NEED',
                        'message_text': 'https://isarbierundsonne.com/2016/05/30/schoenstes-muenchen-kw-21-all-i-need/',
                        'url': 'https://isarbierundsonne.com/2016/05/30/schoenstes-muenchen-kw-21-all-i-need/',
                        'hide_url': False,
                        'description': 'Entertainment'
                        }
                       ]

    return articles_sport2, articles_pol2, articles_enter2
