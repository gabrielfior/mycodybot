import numpy as np

def Deg2Rad( deg ):
	return deg * np.pi / 180


def PythagorasEquirectangular( lat1, lon1, lat2, lon2 ):
	lat1 = Deg2Rad(lat1)
	lat2 = Deg2Rad(lat2)
	lon1 = Deg2Rad(lon1)
	lon2 = Deg2Rad(lon2)
	R = 6371.
	x = (lon2-lon1) * np.cos((lat1+lat2)/2.)
	y = (lat2-lat1)
	d = np.sqrt(x*x + y*y) * R
	return d
