

def read_source1():

    with open('sources.txt','r') as f:
        x = f.readlines()

    url2_list=[]
    tags_list=[]
    coords_list=[]
    for i in x:
        url2 = i.split('?')[0][:-1]
        url2_list.append(url2)
        tags = i.split('?')[1][1:-1]
        tags_list.append(tags)
        coords = [i.split('?')[2][1:-1], i.split('?')[3][1:-1]]
        coords_list.append(coords)



    return url2_list, coords_list

